class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :discounts do |t|
      t.references :product, foreign_key: true, null: true
      t.integer :boundary, null: false, index: true
      t.integer :condition_type, null: false, index: true
      t.decimal :condition_amount, precision: 8, scale: 2, null: false
      t.integer :discount_type, null: false, index: true
      t.decimal :discount_amount, precision: 8, scale: 2, null: false

      t.timestamps
    end
  end
end
