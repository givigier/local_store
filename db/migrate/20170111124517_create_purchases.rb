class CreatePurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :purchases do |t|
      t.decimal :price, precision: 8, scale: 2, null: false, default: 0

      t.timestamps
    end
  end
end
