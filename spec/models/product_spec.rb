require 'rails_helper'

RSpec.describe Product do
  context "when validating" do
    let(:product){ create(:product) }

    it { expect(product).to validate_presence_of(:name) }
    it { expect(product).to validate_presence_of(:price) }
    it { expect(product).to have_many(:discounts) }
    it { expect(product).to accept_nested_attributes_for(:discounts) }
  end
end
