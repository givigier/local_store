require 'rails_helper'

RSpec.describe Purchase do
  context "when validating" do
    let(:purchase){ create(:purchase) }

    it { expect(purchase).to validate_presence_of(:price) }
  end
end
