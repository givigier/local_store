require 'rails_helper'

RSpec.describe Discount do
  context "when validating" do
    let(:discount){ create(:discount) }

    it { expect(discount).to validate_presence_of(:boundary) }
    it { expect(discount).to validate_presence_of(:condition_type) }
    it { expect(discount).to validate_presence_of(:condition_amount) }
    it { expect(discount).to validate_presence_of(:discount_type) }
    it { expect(discount).to validate_presence_of(:discount_amount) }

    context "boundary enum" do
      it { expect(Discount.boundaries[:equal]).to eq 0 }
      it { expect(Discount.boundaries[:less_than]).to eq 1 }
      it { expect(Discount.boundaries[:greater_than]).to eq 2 }
    end

    context "condition_type enum" do
      it { expect(Discount.condition_types[:quantity]).to eq 0 }
      it { expect(Discount.condition_types[:money_spent]).to eq 1 }
    end

    context "discount_type enum" do
      it { expect(Discount.discount_types[:percentage]).to eq 0 }
      it { expect(Discount.discount_types[:money]).to eq 1 }
    end
  end
end
