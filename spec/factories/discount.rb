FactoryGirl.define do
  factory :discount do
    condition_type 0
    condition_amount 3
    boundary 0
    discount_type 0
    discount_amount 0.10

    factory :product_discount do
      association :product
    end
  end
end
