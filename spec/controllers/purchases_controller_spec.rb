require 'rails_helper'

RSpec.describe PurchasesController do
  context "GET #index" do
    let!(:purchase){ create(:purchase) }

    it "populates an array of purchases" do
      get :index
      expect(assigns(:purchases)).to eq [purchase]
    end
  end

  context "GET #new" do
    let!(:product){ create(:product) }
    before(:each){ get :new }

    it "assigns a new Purchase to @purchase" do
      expect(assigns(:purchase)).to be_an_instance_of(Purchase)
    end

    it "assigns an array of Product to @products" do
      expect(assigns(:products)).to eq [product]
    end
  end

  context "POST #create" do
    let!(:product){ create(:product) }

    context "with valid attributes" do
      it "creates a new purchase" do
        expect{
          post :create, params: { products_quantity: { product.id => 3 } }
        }.to change(Purchase,:count).by(1)
      end

      it "redirects to the index" do
        post :create, params: { products_quantity: { product.id => 3 } }
        expect(response).to redirect_to purchases_path
      end
    end
  end
end
