require 'rails_helper'

RSpec.describe ProductsController do
  context "GET #index" do
    let!(:product){ create(:product) }

    it "populates an array of products" do
      get :index
      expect(assigns(:products)).to eq [product]
    end
  end

  context "GET #new" do
    before(:each){ get :new }

    it "assigns a new Product to @product" do
      expect(assigns(:product)).to be_an_instance_of(Product)
    end

    it "assigns a new Discount to @product" do
      expect(assigns(:product).discounts.first).to be_an_instance_of(Discount)
    end
  end

  context "POST #create" do
    context "with valid attributes" do
      it "creates a new product" do
        expect{
          post :create, params: { product: attributes_for(:product) }
        }.to change(Product,:count).by(1)
      end

      it "redirects to the index" do
        post :create, params: { product: attributes_for(:product) }
        expect(response).to redirect_to products_path
      end
    end

    context "with invalid attributes" do
      it "does not save the new product" do
        expect{
          post :create, params: { product: attributes_for(:product, name: nil) }
        }.to_not change(Product,:count)
      end

      it "re-renders the new method" do
        post :create, params: { product: attributes_for(:product, name: nil) }
        expect(response).to render_template :new
      end
    end
  end
end
