require 'rails_helper'

RSpec.describe Discounts::Validator do
  let(:validator){ Discounts::Validator.new(100, discount) }

  context "when boundary is equal" do
    context "and condition_amount is equal to amount" do
      let(:discount){ build_stubbed(:discount, boundary: "equal", condition_amount: 100) }

      it "return true" do
        expect(validator.valid?).to be_truthy
      end
    end

    context "and condition_amount is not equal to amount" do
      let(:discount){ build_stubbed(:discount, boundary: "equal", condition_amount: 101) }

      it "return false" do
        expect(validator.valid?).to be_falsey
      end
    end
  end

  context "when boundary is less_than" do
    context "and condition_amount is less than amount" do
      let(:discount){ build_stubbed(:discount, boundary: "less_than", condition_amount: 101) }

      it "return true" do
        expect(validator.valid?).to be_truthy
      end
    end

    context "and condition_amount is not less than amount" do
      let(:discount){ build_stubbed(:discount, boundary: "less_than", condition_amount: 100) }

      it "return false" do
        expect(validator.valid?).to be_falsey
      end
    end
  end

  context "when boundary is greater_than" do
    context "and condition_amount is greater than amount" do
      let(:discount){ build_stubbed(:discount, boundary: "greater_than", condition_amount: 99) }

      it "return true" do
        expect(validator.valid?).to be_truthy
      end
    end

    context "and condition_amount is not greater than amount" do
      let(:discount){ build_stubbed(:discount, boundary: "greater_than", condition_amount: 100) }

      it "return false" do
        expect(validator.valid?).to be_falsey
      end
    end
  end
end
