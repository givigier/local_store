require 'rails_helper'

RSpec.describe Discounts::Calculator do
  let(:calculator){ Discounts::Calculator.new(100, discount) }

  context "when discount_type is percentage" do
    let(:discount){ build_stubbed(:discount, discount_type: "percentage", discount_amount: 10) }

    it "get the price with 10% of discount" do
      expect(calculator.get_price_with_discount).to eq 90.0
    end
  end

  context "when discount_type is money" do
    let(:discount){ build_stubbed(:discount, discount_type: "money", discount_amount: 15) }

    it "get the price with $15 of discount" do
      expect(calculator.get_price_with_discount).to eq 85.0
    end
  end
end
