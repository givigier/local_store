require 'rails_helper'

RSpec.describe Discounts::Product do
  let(:discount_product){ Discounts::Product.new(product, 2) }

  context ".get_latest_discount" do
    let!(:product){ create(:product) }
    let!(:latest_discount) do
      create(:discount, condition_type: "quantity", condition_amount: 3,
        boundary: "less_than", created_at: DateTime.now - 2.days, product_id: product.id)
    end
    let!(:invalid_discount) do
      create(:discount, condition_type: "quantity", condition_amount: 2,
        boundary: "less_than", product_id: product.id)
    end
    let!(:valid_discount) do
      create(:discount, condition_type: "quantity", condition_amount: 3,
        boundary: "less_than", created_at: DateTime.now - 3.days, product_id: product.id)
    end

    it "return the latest valid discount" do
      expect(discount_product.get_latest_discount).to eq latest_discount
    end
  end

  context ".extract_product_amount" do
    context "when condition_type is quantity" do
      let(:product){ build_stubbed(:product) }
      let(:discount){ build_stubbed(:discount, condition_type: "quantity") }

      it "return the quantity" do
        expect(discount_product.extract_product_amount(discount)).to eq 2
      end
    end

    context "when condition_type is money_spent" do
      let(:product){ build_stubbed(:product, price: 10) }
      let(:discount){ build_stubbed(:discount, condition_type: "money_spent") }

      it "return the sum of products price" do
        expect(discount_product.extract_product_amount(discount)).to eq 20
      end
    end
  end
end
