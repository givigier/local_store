require 'rails_helper'

RSpec.describe PurchaseCalculator do
  let(:product){ create(:product, price: 15) }
  let(:calculator){ PurchaseCalculator.new({ product.id => 3 }) }

  context "when there is a discount" do
    let!(:discount) do
      create(:discount,
        product_id: product.id,
        boundary: "greater_than",
        condition_type: "quantity",
        condition_amount: 2,
        discount_type: "money",
        discount_amount: "5"
      )
    end

    it "return sum of products price with discount" do
      expect(calculator.calculate_price).to eq 40.0
    end
  end

  context "when there is not a discount" do
    it "return the sum of products price" do
      expect(calculator.calculate_price).to eq 45.0
    end
  end
end
