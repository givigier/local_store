class PurchasesController < ApplicationController
  def index
    @purchases = Purchase.all
  end

  def new
    @products = Product.all
    @purchase = Purchase.new
  end

  def create
    @purchase = Purchase.new
    @purchase.price = PurchaseCalculator.new(params[:products_quantity]).calculate_price
    @purchase.save
    redirect_to purchases_path
  end
end
