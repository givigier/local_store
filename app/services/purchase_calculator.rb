class PurchaseCalculator
  def initialize(products_quantity)
    @products = products_quantity.select{ |id, quantity| quantity.to_i > 0 }
  end

  def calculate_price
    price = 0.0
    @products.each do |product_id, quantity|
      product = Product.find(product_id)
      discount = Discounts::Product.new(product, quantity).get_latest_discount
      total_price = product.price.to_d * quantity.to_d
      price += Discounts::Calculator.new(total_price, discount).get_price_with_discount
    end
    price
  end
end
