class Discounts::Product
  def initialize(product, quantity)
    @product = product
    @quantity = quantity
  end

  def get_latest_discount
    discounts = []
    @product.discounts.each do |discount|
      product_amount = extract_product_amount(discount)
      discounts << discount if Discounts::Validator.new(product_amount, discount).valid?
    end
    discounts.sort_by(&:created_at).last
  end

  def extract_product_amount(discount)
    case discount.condition_type
    when "quantity" then @quantity.to_d
    when "money_spent" then @product.price.to_d * @quantity.to_d
    else nil end
  end
end
