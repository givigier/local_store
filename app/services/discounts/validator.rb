class Discounts::Validator
  def initialize(amount, discount)
    @amount = amount
    @discount = discount
  end

  def valid?
    case @discount.boundary
    when "equal" then @amount == @discount.condition_amount
    when "less_than" then @amount < @discount.condition_amount
    when "greater_than" then @amount > @discount.condition_amount
    else nil end
  end
end
