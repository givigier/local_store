class Discounts::Calculator
  def initialize(total_price, discount)
    @total_price = total_price
    @discount = discount
  end

  def get_price_with_discount
    case @discount&.discount_type
    when "percentage" then @total_price - ((@discount.discount_amount.to_d/100) * @total_price)
    when "money" then @total_price - @discount.discount_amount
    else @total_price end
  end
end
