class Discount < ApplicationRecord
  enum boundary: [:equal, :less_than, :greater_than]
  enum condition_type: [:quantity, :money_spent]
  enum discount_type: [:percentage, :money]

  validates :boundary, presence: true
  validates :condition_type, presence: true
  validates :condition_amount, presence: true
  validates :discount_type, presence: true
  validates :discount_amount, presence: true
end
