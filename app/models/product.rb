class Product < ApplicationRecord
  validates :name, presence: true
  validates :price, presence: true

  has_many :discounts

  accepts_nested_attributes_for :discounts
end
